# anon.w0rk

*Anon IDE*

***
<br>

### Description
This is the Anon Integrated Development Environment. It provides the back-end and front-end functionality libraries and also a GUI to build and manage your project.
You can access the w0rk-panel (GUI) in your web browser by visiting the URL where Anon is installed and press the `Ctrl Meta x` keys together on your keyboard.
<br>

### Installation
This feature is automatically installed when you install the Anon framework on your server.
<br>


### Documentation
This feature's documentation ***is*** the **Anon Tutorial** and it's available in the `doc` folder in `markdown` format.<br>
You can view this documentation as web pages in your browser by using the "docs" button (book icon) on the w0rk-panel's side-menu.

Via browser interface, you can only view the documentation on your own Anon server if you are logged in as any `hacker`, `stager`, or `master`.

To log in as a power user:
- visit your website in your favorite browser
- using your keyboard, press: `Ctrl Meta x` -together, which pops up the *w0rk* panel
- in the terminal, type in: `su supra` and hit the `Enter` key on your keyboard
- it asks for a password, type in `0m1cr0n` and hit `Enter` again.

The username and password provided above are the defaults for the *Super User* in every new Anon installation; however directly after installation it is mandatory to change the `Super User` password and/or username; so if this has already been changed, replace the username and password in the instructions above with your own instead.

Once you are logged in, you have access to various w0rk features, including the Anon Manual.
Your user may be limited to certain features according to your *roles* and *rank*.
<br>


### Contribution
The development principles of this feature is: **CANDRYKIS**
- Compartmentalized And Nomadic .. or canonically organized
- Don't Repeat Yourself
- Keep It Simple
