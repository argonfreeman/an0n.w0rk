
// w0rk :: conf : tool
// --------------------------------------------------------------------------------------------------------------------------------------------
   requires(['/w0rk/conf/skin.css'],()=>{});

   Extend(w0rk)
   ({
      conf:
      {
         vars:{active:undefined},


         init:function()
         {
         },
      }
   });
// --------------------------------------------------------------------------------------------------------------------------------------------




// w0rk :: conf : slab
// --------------------------------------------------------------------------------------------------------------------------------------------
   Export
   ([
      {grid:'#confD3vlPanl .w0rkPanlSlab', onready:function(){w0rk.conf.init()}, contents:
      [
         {row:
         [
            {col:'.panlSlabMenu', contents:
            [
               {grid:
               [
                  {row:[{col:'.slabMenuHead', contents:'config'}]},
                  {row:[{col:'.panlHorzLine', contents:{hdiv:''}}]},
                  {row:[{col:'.slabMenuBody', contents:'',}]},
               ]}
            ]},
            {col:'.panlVertDlim', contents:{vdiv:'.moveHorz', target:'^<'}},
            {col:'.panlSlabView', contents:
            [
               {grid:
               [
                  {row:[{col:'.slabViewHead', contents:[]}]},
                  {row:[{col:'.panlHorzLine', contents:{hdiv:''}}]},
                  {row:[{col:'.slabViewBody', contents:[{panl:'#w0rkTaskView'}]}]},
               ]}
            ]},
         ]}
      ]}
   ]);
// --------------------------------------------------------------------------------------------------------------------------------------------
