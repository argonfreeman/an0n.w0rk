"use strict";



// tool :: w0rk : feature
// --------------------------------------------------------------------------------------------------------------------------------------------
   Extend(Main)
   ({
      w0rk:{},


      w0rkMenu:
      {
         vars:{active:VOID},

         init:function(id, ot,op,nt,np)
         {
            Select('#w0rkView').render('block'); ot=this.vars.active; nt=id.frag(0,4);
            if(ot){Select('#'+ot+'D3vlKnob').declan('w0rkActvKnob'); op=Select('#'+ot+'D3vlPanl');if(op){op.parentNode.removeChild(op);}};
            Select('#'+id).enclan('w0rkActvKnob'); this.vars.active=nt; np=Select('#'+nt+'D3vlPanl');
            Render(('/w0rk/'+nt+'/init'),Select('#w0rkMainView'),()=>{});
         },
      },



      w0rktask:
      {
         open:function()
         {
            requires(['/c0r3/lib/edit/mrkd.js','/c0r3/lib/edit/prsm.js','/c0r3/lib/edit/mkdn.css','/c0r3/lib/edit/prsm.css'],()=>
            {

               let md='<a href="https://www.markdownguide.org/cheat-sheet/#basic-syntax" target="_blank">Markdown</a>';
               modal
               ({
                  name:'#newDocket',
                  head:'New Docket',

                  body:
                  [
                     {p:'Kindly fill in your message details below and hit Create.'},
                     {ul:
                     [
                        {li:'You can use '+md+' syntax, but it\'s optional. Feel free to just type -or paste & edit your message.'},
                        {li:'Files you drag/drop will be inserted as dynamically linked references where you are typing.'},
                        {li:'Please be nice, else your ticket may be ignored automatically.'},
                        {li:'To preview, hit the <b>View</b> tab below.'},
                     ]},
                     {input:'#w0rkDoktfromName', type:(userIs('.worker')?'hidden':'text'), placeholder:'nickname', required:true,
                        value:(userIs('.worker')?User('name'):''), pattern:/^[a-zA-Z][a-zA-Z0-9-_]{2,20}$/,
                     },
                     {input:'#w0rkDoktfromAddy', type:(userIs('.worker')?'hidden':'text'), placeholder:'me@example.com', required:true,
                        value:(userIs('.worker')?User('mail'):''), pattern:/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/,
                     },
                     {select:'#w0rkDoktpriority', style:('display:'+(userIs('.worker')?'block':'none')), contents:
                     [
                        {option:'normal'},
                        {option:'urgent'},
                        {option:'threat'},
                     ]},
                     {tabs:'#w0rkDoktTabs', selected:0, contents:
                     [
                        {Edit:
                        [
                           {textarea:'#w0rkDoktmesgBody', placeholder:'enter text and drop files here', pattern:/^[\S\s]{10,12000}$/,
                              required:true,
                              target:'^2',
                              onfeed:function(v,f, n,x,self,vars)
                              {
                                 x=f.rstub('.'); if(!x){alert('invalid image name: missing file-extension');return}; n=x[0]; x=x[2];
                                 if(!(/^[a-zA-Z0-9_\.-]{3,40}$/).test(f)){n=md5(n); f=(n+'.'+x);}; self=this;
                                 vars={type:'upload',file:f,durl:v,dref:self.dref,cref:self.cref};
                                 purl({target:'/w0rk/task/make',method:'POST',convey:vars},function()
                                 {
                                    let il=['jpg','jpeg','png','svg','gif','bmp']; let a=(il.hasAny(x)?'!':'');
                                    self.insertAtCaret(a+'['+n+']('+f+')'); let ro=JSON.parse(this.echo.body);
                                    self.dref=ro.dref; self.cref=ro.cref;
                                 });
                              },
                              onblur:function()
                              {
                                 var trgt,path,text,imgl,bw,ew,mb,dr,cr; trgt=Select('#w0rkDoktView'); trgt.innerHTML='';
                                 text=(this.value||'&nbsp;<br><br><br>&nbsp;'); mb=Select('#w0rkDoktmesgBody'); dr=mb.dref; cr=mb.cref;
                                 bw='!['; ew=')'; imgl=text.expose(bw,ew); if(!imgl){imgl=[]}; path=('/w0rk/task/list/'+dr+'/comments/'+cr);
                                 imgl.forEach((i)=>
                                 {let f=(bw+i+ew); let p=i.stub(']('); let r=(bw+p[0]+']('+path+'/'+p[2]+ew); text=text.swap(f,r)});
                                 Render(text,trgt,'md',()=>{});
                              },
                           },
                        ]},
                        {View:
                        [
                           {div:'#w0rkDoktView', //onready:function(){Select('#w0rkDoktmesgBody').Signal('blur')}
                           },
                        ]},
                     ]},
                  ],

                  foot:
                  [
                     {butn:'', contents:'Cancel', onclick:function(){Select('#modalView').Delete()}},
                     {butn:'.Good', contents:'Create', onclick:function(v)
                     {
                        let l=['fromName','fromAddy','priority','mesgBody']; v=VOID; v={}; let pass=true; l.Each((i)=>
                        {let n=i; let o=Select('#w0rkDokt'+i); if(o.fail){pass=false; o.Signal('blur'); return STOP}; v[n]=o.value;});
                        if(!pass){return}; let mb=Select('#w0rkDoktmesgBody'); v.dref=mb.dref; v.cref=mb.cref; v.type='create';
                        v.mesgHead=(window.top.document.title+'').trim(); w0rktask.make(v,function(resp)
                        {
                           if(resp=='OK'){let mdl = Select('#modalView'); if(mdl){mdl.Delete()};  return};
                           alert(resp);
                        });
                     }},
                  ]
               });


            });
         },


         make:function(v,f)
         {
            purl({target:'/w0rk/task/make',method:'POST',convey:v},function()
            {
               if(isFunc(f)){f(this.echo.body)};
            });
         },
      },



      w0rkPanl:
      {
         show:function()
         {
            Select('#w0rkView').render('block'); Select('#w0rkMainPanl').style.display=(User('role').hasAny('worker')?'block':'none');
            this.actv=1; Select('#replFeed').focus();
         },
         hide:function(){Select('#w0rkView').render('none'); this.actv=0;},
         actv:0,
      },
   });
// --------------------------------------------------------------------------------------------------------------------------------------------




// init :: panl : view
// --------------------------------------------------------------------------------------------------------------------------------------------
   requires(['/w0rk/aard.css','/c0r3/fnt/code.fnt','/c0r3/fnt/head.fnt','/c0r3/fnt/butn.fnt'],()=>
   {
      Select('#w0rkView').Insert
      ([
         {grid:
         [
            {row:
            [
               {col:
               [
                  {panl:'#w0rkMainPanl', style:('display:'+(User('role').hasAny('worker')?'block':'none')), contents:
                  [
                     {grid:'', style:'width:100%; height:100%', contents:
                     [
                        {row:
                        [
                           {col:'#w0rkMainMenu',contents:(function()
                           {
                              var mods={:(mods):}; var btns=[];
                              mods.Each((v,k)=>
                              {
                                 btns.push({butn:('#'+k+'D3vlKnob .w0rkMainButn .'+v), title:k, onclick:function(){w0rkMenu.init(this.id)}});
                              });
                              return btns;
                           }())},
                           {col:'.panlVertDlim', contents:{vdiv:''}},
                           {col:[{panl:'#w0rkMainView'}]},
                        ]},
                     ]},
                  ]},
               ]}
            ]},
            {row:
            [
               {col:'.panlHorzDlim .moveVert', target:'#w0rkReplPanl', contents:{hdiv:''}},
            ]},
            {tr:'#w0rkReplPanl', contents:
            [
               {td:[{panl:'', style:'overflow:hidden', contents:from('/c0r3/lib/repl/repl.htm')}]},
            ]},
         ]}
      ]);
   });
// --------------------------------------------------------------------------------------------------------------------------------------------




// togl :: show/hide
// --------------------------------------------------------------------------------------------------------------------------------------------
   Listen('Control `',function()
   {
      if(!w0rkPanl.actv){w0rkPanl.show(); return};
      w0rkPanl.hide();
   });
// --------------------------------------------------------------------------------------------------------------------------------------------




// togl :: hide : on-escape
// --------------------------------------------------------------------------------------------------------------------------------------------
   Listen('key:Esc',function(evnt)
   {
      let mdl = Select('#modalView'); if(mdl){mdl.Delete(); return};
      w0rkPanl.hide();
   });
// --------------------------------------------------------------------------------------------------------------------------------------------




// evnt :: create : docket
// --------------------------------------------------------------------------------------------------------------------------------------------
   Listen('Control Enter',function()
   {
      w0rktask.open();
   });
// --------------------------------------------------------------------------------------------------------------------------------------------
