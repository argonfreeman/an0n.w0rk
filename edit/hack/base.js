"use strict";


Extend(w0rk.edit)
({
   hack:
   {
      vars:{},
      bufr:{},
      conf:{mxkb:500},
      init:['/c0r3/lib/edit/prsm.js','/c0r3/lib/edit/prsm.css','/w0rk/edit/hack/skin.css'],
      mime:{js:'javascript', jsm:'javascript', htm:'html', txt:'text', md:'markdown'},
      lang:'text',


      prep:function()
      {
         let crs=Select('#w0rkEditCrsr'); crs.style.display='inline-block';
         let txt=Select('#w0rkEditText'); txt.focus();
         let pos=txt.selectionStart; let pts=txt.value.substr(0,pos).split('\n');
         let row=pts.length; let col=(pts[(row-1)].length+1); row-=1; col-=1;
         crs.style.top=((row*14)+'px'); crs.style.left=(((col*6)+38)+'px');
      },


      fidl:function()
      {
         var p,b,l,n,i,h; p=this.vars.active; b=this.bufr[p]; Select('#w0rkEditSkin').textContent=(b+'\n');
         Select('#w0rkEditDeck').className=('language-'+this.lang+' line-numbers');
         Select('#w0rkEditSkin').className=('language-'+this.lang);
         Prism.highlightAllUnder(Select('#w0rkEditDeck').parentNode);
         Select('#w0rkEditDeck').style.background='transparent'; Select('#w0rkEditText').style.background='transparent';
         h=''; n=Select('#w0rkEditGutr'); l=Select('#w0rkEditDeck .line-numbers-rows')[0].childNodes.length;
         this.prep(); n.innerHTML=''; for(i=0;i<l;i++){h+='<span></span>';}; n.innerHTML=h;
      },


      open:function(p,x, self)
      {
         self=this; purl({target:'/w0rk/edit/hack/open', method:'POST', convey:{path:p}},function()
         {
            var s,l,t,b;  s=(['md','txt'].hasAny(x)?true:false); l=(self.mime[x]||x); self.lang=l;

            Select('#w0rkEditView').style.overflow='hidden'; Select('#w0rkEditView').Insert
            ([
               {div:'#w0rkEditCode',contents:
               [
                  {pre:'#w0rkEditDeck',contents:[{code:'#w0rkEditSkin'}]}, {div:'#w0rkEditCrsr'},
                  {textarea:'#w0rkEditText', autocomplete:"off", autocorrect:"off", autocapitalize:"off", spellcheck:s},
                  {div:'#w0rkEditGutr .gutrNmbr'},
               ]}
            ]);

            t=Select('#w0rkEditText'); t.Listen('focus',(evnt)=>{evnt.Target.hasFocus=true;});
            t.Listen('blur',(evnt)=>{evnt.Target.hasFocus=false;});
            t.Listen(['mousedown','mouseup'],()=>{w0rk.edit.hack.prep();});
            t.Listen('scroll',(evnt)=>
            {
               let code,self; code=Select('#w0rkEditDeck'); self=evnt.Target;   let tst=self.scrollTop; let tsl=self.scrollLeft;
               code.scrollTop=tst; code.scrollLeft=tsl; let cst=code.scrollTop; let csl=code.scrollLeft;
               self.scrollTop=cst; self.scrollLeft=csl;
            });

            t.Listen(['keydown','keyup'],(evnt)=>
            {var p,s; p=w0rk.edit.hack.vars.active; s=evnt.Target; w0rk.edit.hack.bufr[p]=s.value; w0rk.edit.hack.fidl();});

            t.Listen('Control Meta s',()=>{w0rk.edit.hack.save()});

            self.timr=setInterval(function()
            {
               let crsr = Select('#w0rkEditCrsr'); if(!crsr){clearInterval(self.timr); return};
               if(!Select('#w0rkEditText').hasFocus){crsr.style.display='none'; return};
               let show = cStyle(crsr,'display'); crsr.style.display=((show=='none')?'inline-block':'none');
            },360);

            b=this.echo.body; self.bufr[p]=b; t.value=b; self.vars.active=p; self.fidl();
         });
      },


      save:function()
      {
         let p,b; p=this.vars.active; b=this.bufr[p]; purl({target:'/w0rk/edit/hack/save', method:'POST', convey:{path:p,bufr:b}},function()
         {
            let r=this.response; if(r=='OK'){return}; fail(r);
         });
      },
   }
});
