<?
namespace Anon;



$init=function()
{
   $h='/w0rk/edit'; $l=path::scan($h,TRON,DEEP,[FOLD,'*.inf']); $r=tron();
   foreach($l as $k => $v){$i=keys($v->kids)[0]; $t=frag(trim(path::scan("$h/$k/$i"))," "); foreach($t as $x){$r->$x=$k;};};

   render::parsed('/w0rk/edit/base.js',['mime'=>encode::json($r)]);
};



$menu=tron
([
   'init'=>function()
   {
      $i=target('git::/'); if(!isRepo('/')){$i->create();}; $n=sha1('/'); $u=USERNAME; $m=USERMAIL; $p="/w0rk/user/$u/repo/$n";
      if(!isRepo($p)){$p=$i->deploy($p);}else{$p="git::$p";}; $i=target($p); $l=$i->select([fetch=>'*']);
      if(span($l)<1){emit('{}');}; $r=tron(); foreach($l as $i)
      {
         $t=$i->type; $i->mime=$t; if($t==='fldr'){$i->kids=tron();}else{$i->type='file';}; $p=trim($i->path,'/'); $p=swap($p,'/','/kids/');
         $i->diff=$i->stat; unset($i->stat); bore($r,"/$p",$i);
      };
      render::direct($r);
   },


   'feed'=>function()
   {
      $v=vars('client'); $p=path::w0rk($v->path); $f=$v->file; $d=$v->data; $x=path($p); $z="$v->path{$f}";
      if(isFold($x)){if(!is_writable($x)){emit("`$z` is not writable");}; $p="$p/$f"; $x=path($p);};
      if(isFile($x)&&(path::size($x)>0)){emit("`$z` is not empty");}; $r=path::make($p,$d['body'],FILE);
      if(!$r){emit("failed to upload `$z`");}; emit('OK');
   },


   'item'=>
   [
      'idea'=>function()
      {
         $v=vars('client'); $d=$v->twig;
         $f=$v->leaf; $t=":$v->type:"; $h=path::w0rk($d); $p="$h/$f"; if(!isFold($h)){emit("`$d` is not a folder");};
         if(!is_writable(path($h))){emit("`$d` is not writable");}; $x=swap("$d/$f",'//','/'); if(exists($p,PATH)){emit("`$x` already exists");};
         $d=''; if($t===FOLD){$p="$p/.gitkeep"; $d='!'; $t=FILE;}; $r=path::make($p,$d,$t); if(!$r){emit("failed to create `$x`");};
         exec::{"git add --all"}($h); exec::{"git commit -am \"created $x\""}($h); emit('OK');
      },
   ],
]);
