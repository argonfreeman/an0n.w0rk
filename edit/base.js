"use strict";

// w0rk :: edit : tool
// --------------------------------------------------------------------------------------------------------------------------------------------
   requires(['/w0rk/edit/skin.css','/c0r3/fnt/code.fnt'],()=>{});

   Extend(w0rk)
   ({
      edit:
      {
         mime:{:(mime):},
         menu:
         {
            vars:{lastInit:0,fetching:0},



            init:function(self,vars,time)
            {
               self=this; time=Math.floor(Date.now()/1000); if(((time-self.vars.lastInit)<1)||self.vars.fetching){return};
               if(!Select('#editD3vlPanl')||(cStyle(Select('#w0rkView'),'display')=='none')){return}; self.vars.lastInit=time;
               self.vars.fetching=1; purl({target:'/w0rk/edit/menu/init',silent:1},function()
               {
                  if(this.status!==200){let dbug=JSON.parse(this.responseText); cookie.create('dbugInfo',dbug); halt(); return;};
                  let l,r; l=self.item.prep(decode.JSON(this.echo.body)); r=[]; l.Each((i)=>{r.push(self.item.make(i))});
                  let m=Select('#editD3vlPanl .slabMenuBody')[0]; m.innerHTML=''; m.Insert(r); self.vars.fetching=0;
               });
            },



            cntx:
            {
               show:function(evnt, func,trgt,cntx,menu,clan)
               {
                  clan='w0rkDropMenu'; trgt=evnt.target; let tn='pathitem';
                  trgt=(((trgt.className=='slabMenuBody')||!!trgt.info)?trgt:((nodeOf(trgt.Select(0))==tn)?trgt.Select(0):trgt.Parent(tn)));
                  cntx=((trgt.info.path=='/')?'root':trgt.info.type);
                  func=function(cmnd){w0rk.edit.menu.cntx.cmnd(cmnd,this.trgt);}.bind({trgt:trgt});

                  if(cntx=='root'){dropMenu
                  ([
                     ['create child','new file/folder in root',func],
                     '---',
                     ['save all changes','auto-sequence: add commit pull push',func],
                     ['stash set changes','temporary save without commit',func],
                     ['stash get changes','impose stashed changes in here',func],
                     ['purge all changes','remove any uncommitted changes',func],
                     '---',
                     ['undo - revert back','restore older commit',func],
                     ['redo - revert next','restore newer commit',func],
                     ['jump - revert pick','restore other commit',func],
                     '---',
                     ['import feature','clone/fork a remote repo into a sub-folder',func],
                     ['import project','clone/fork remote repo as new project',func],
                     '---',
                     ['export project','download entire project as a zip package',func],
                     ['publish project','install this project on another server via FTP',func],
                  ],evnt,clan);return};

                  if(cntx=='fldr'){dropMenu
                  ([
                     ['create child','new file/folder in this folder',func],
                     ['create sibling','new file/folder in parent folder',func],
                     '---',
                     ['copy this','create renamed duplicate',func],
                     ['rename this','give this another name',func],
                     ['delete this','remove this item',func],
                  ],evnt,clan);return};

                  dropMenu
                  ([
                     ['hack this','open in hack editor',func],
                     ['draw this','open in draw editor',func],
                     ['plan this','open in plan editor',func],
                     '---',
                     ['create sibling','new file/folder in parent folder',func],
                     ['copy this','create renamed duplicate',func],
                     ['rename this','give this another name',func],
                     ['delete this','remove this item',func],
                  ],evnt,clan);
               },


               cmnd:function(cmnd,trgt, info,item,menu,home)
               {
                  menu=w0rk.edit.menu; info=trgt.info; home=((info.path=='/')?trgt:trgt.Select('>>'));
                  if(cmnd=='create child')
                  {
                     if(info.path!='/'){info.levl+=1; if(!w0rk.edit.menu.item.vars.opened[info.path])
                     {w0rk.edit.menu.item.bang({info:info,Target:trgt})}};
                     item=Create(menu.item.make({levl:info.levl,path:info.path,type:'auto',text:'',icon:'diff'},1));
                     home.Insert(item,0); setTimeout(()=>{item.Select('input')[0].focus();},100); return;
                  };
                  dump(cmnd,trgt);
               },
            },



            exec:function(path,data,func)
            {
               purl({target:('/w0rk/edit/menu/'+path), method:'POST', convey:data, listen:
               {
                  loadend:function(){w0rk.edit.menu.init(); if(isFunc(func)){func(this.response)};},
               }});
            },



            feed:function(evnt, trgt)
            {
               trgt=evnt.target; let tn='pathitem'; if(trgt.className!='slabMenuBody')
               {trgt=((nodeOf(trgt.Select(0))==tn)?trgt.Select(0):trgt.Parent(tn))};
               this.exec('feed',{path:trgt.info.path,file:evnt.detail.name,data:evnt.detail.data},function(resp)
               {
                  if(resp!='OK'){trgt.notify(resp,FAIL,LT);return}; //w0rk.edit.menu.init();
               });
            },



            item:
            {
               vars:{opened:{},active:VOID},


               icon:
               {
                  auto:'file',
                  fldr:'file-directory',
                  none:'file-empty',
                  jpg:'file-media',
                  png:'file-media',
                  gif:'file-media',
                  ico:'file-media',
               },


               prep:function(y,l,p)
               {
                  var self,d,f,r; self=this; d=[]; f=[]; r=[]; if(l===undefined){l=-1}; l+=1; let a=self.icon.auto; if(!p){p=''};
                  y.Each((v,k)=>
                  {
                     let t=v['type']; let i={levl:l,path:(p+'/'+k),type:t,text:k}; i.mime=v['mime']; i.icon=(self.icon[i.mime]||a);
                     i.kids=(v['kids']?self.prep(v['kids'],l,(p+'/'+k)):null); i.size=v['size']; i.diff=v['diff'];
                     i.repo=v['repo']; if(i.repo){i.icon=((i.repo.fork=='master')?'repo-clone':'repo-forked');};
                     if(i.type=='fldr'){d[d.length]=i}else{f[f.length]=i};
                  });
                  r=d.concat(f); return r;
               },


               make:function(i,s, self)
               {
                  var p,r,n,e,kd,mc,sz; p=16; r=''; n=('pathItem'+md5(i.path));
                  sz=(((i.type=='fldr')&&(span(i.kids)<1))?'(empty)':(i.size+' Kb'));
                  e=(s?'press Enter to confirm, or Esc to cancel':(i.repo?(i.repo.repo+'/'+i.repo.fork+' '+sz):(i.mime+' '+sz)));
                  self=this; let o=self.vars.opened[i.path]; let k=('display:'+(o?'block':'none')); let y=(s?'.newPathItem':'');
                  let t=((i.type=='fldr')?('chevron-'+(o?'down':'right')):(s?'plus':'')); let c=(i.diff?('.diff'+i.diff):'');
                  if(s){kd=function(e,b){w0rk.edit.menu.item.idea(e)}}else{mc=function(e){w0rk.edit.menu.item.bang(e)}};
                  let os=(s?function(){}:function(){dump(this.value);}); y=y.trim(); if((i.type=='fldr')&&!i.kids){i.kids=[]};
                  r={pathnode:
                  [{
                     pathitem:('#'+n+' '+c).trim(), info:{path:i.path,size:i.size,type:i.type,levl:i.levl,mime:i.mime}, title:e,
                     ondragstart:function(e){e.dataTransfer.setData('text/plain',this.info.path);}, onclick:mc, contents:[{grid:[{row:
                     [
                        {itemcell:'', style:('width:'+(i.levl*p)+'px')},{itemcell:{icon:t}},{itemcell:{icon:i.icon}},
                        {itemcell:{input:(c+y),placeholder:'new file.ext or folder/',value:i.text,disabled:(s?false:true),title:e,
                           on_keydown:kd,
                        }},
                     ]}]}]
                  }]};
                  if(i.type=='fldr'){r.pathnode.push({pathkids:'', style:k, contents:(function()
                  {let z=[]; i.kids.Each((x)=>{z.push(self.make(x))}); return z;}())})};
                  return r;
               },


               bang:function(e, t,i,k,o,b,s)
               {
                  t=e.Target; i=t.info; if(i.type!='fldr'){this.open(t.info); return}; k=t.Select('>'); o=k.render('block','none');
                  s='icon-chevron-'; this.vars.opened[i.path]=o; b=t.Select("[class^='"+s+"']")[0]; b.className=(s+(o?'down':'right'));
               },


               idea:function(e, s,t,i,p,v,k)
               {
                  s=e.signal; t=e.Target; i=t.Parent('pathitem'); if(s=='Escape'){i.Delete(); return}; if(s!='Enter'){return};
                  p=i.info.path; v=(t.value+'').trim();  k=v.substr(-1,1); if(k=='/'){k='FOLD';v=v.rtrim('/')}else{k='FILE'};
                  if(!v||!v.match(/^[a-zA-Z0-9-\._]{1,36}$/)){t.notify('invalid name',FAIL,LT);return};
                  w0rk.edit.menu.exec('item/idea',{twig:p,leaf:v,type:k},function(r)
                  {
                     if(r!='OK'){t.notify(r,FAIL,LT);return}; //w0rk.edit.menu.init();
                  });
               },


               open:function(p,a, m,s,i,x)
               {
                  if(isTron(p)){p=p.path}; if(!isPath(p)){fail('expecting path');return}; m=(!p.hasAny('.')?VOID:p.split('.').pop());
                  if(!a){if(!m){fail('I have no idea what to do with this: '+p);return}; a=w0rk.edit.mime[m]};
                  if(!a){fail('no tool defined for file-type: '+m);return};
                  if(!w0rk.edit[a]){s=this; requires(('/w0rk/edit/'+a+'/base.js'),()=>{setTimeout(()=>{s.open(p)},250)});return};
                  Select('#w0rkEditView').innerHTML=''; Select('#w0rkEditTool').innerHTML=''; //w0rk.edit[a].vars={};
                  i=w0rk.edit[a].init; if(!i){w0rk.edit[a].open(p,m);return};
                  if(isFunc(i)){i(()=>{w0rk.edit[a].open(p,m)});return}; requires(i,()=>{w0rk.edit[a].open(p,m)});
               },
            },
         },
      }
   });
// --------------------------------------------------------------------------------------------------------------------------------------------




// w0rk :: edit : slab
// --------------------------------------------------------------------------------------------------------------------------------------------
   Export
   ([
      {grid:'#editD3vlPanl .w0rkPanlSlab', onready:function(){w0rk.edit.menu.init()}, contents:
      [
         {row:
         [
            {col:'.panlSlabMenu', contents:
            [
               {grid:
               [
                  {row:[{col:'.slabMenuHead',contents:'editor'}]},
                  {row:[{col:'.panlHorzLine',contents:{hdiv:''}}]},
                  {row:[{col:'.slabMenuBody', info:{path:'/',type:'fldr',size:0,levl:0}, contents:[],
                     on_contextmenu:function(e){w0rk.edit.menu.cntx.show(e)},
                     on_drop:function(e){w0rk.edit.menu.feed(e)},
                  }]},
               ]}
            ]},
            {col:'.panlVertDlim', contents:{vdiv:'.moveHorz', target:'^<'}},
            {col:'.panlSlabView', contents:
            [
               {grid:
               [
                  {row:[{col:'.slabViewHead',contents:[]}]},
                  {row:[{col:'.panlHorzLine',contents:{hdiv:''}}]},
                  {row:[{col:'.slabViewBody',contents:[{panl:'#w0rkEditView'}]}]},
               ]}
            ]},
            {col:'.panlVertDlim', contents:{vdiv:'.moveHorz', target:'^<'}},
            {col:'.panlSlabTool', contents:
            [
               {grid:
               [
                  {row:[{col:'.slabToolHead',contents:'tools'}]},
                  {row:[{col:'.panlHorzLine',contents:{hdiv:''}}]},
                  {row:[{col:'.slabToolBody',contents:[{panl:'#w0rkEditTool'}]}]},
               ]}
            ]},
         ]}
      ]}
   ]);
// --------------------------------------------------------------------------------------------------------------------------------------------
