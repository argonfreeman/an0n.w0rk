"use strict";


Extend(w0rk.edit)
({
   draw:
   {
      vars:{},
      conf:{mxkb:9000},
      init:['/c0r3/lib/edit/konva.min.js','/w0rk/edit/draw/skin.css'],
      load:function(p,f)
      {
         purl({target:'/w0rk/edit/draw/open', method:'POST', convey:{path:p}},function()
         {
            Create({img:this.echo.body, purl:p, onload:f});
            // let l=(new Konva.Image(i));
            // dump(i.naturalWidth,i.naturalHeight);
         });
      },


      open:function(p,x, self,panl)
      {
         Select('#w0rkReplPanl').style.height='0px'; this.vars.file={path:p,type:x};
         panl=Select('.slabViewBody')[0].getBoundingClientRect();
         self=this; self.load(p,function()
         {
            Select('#w0rkEditView').Insert
            ([
               {div:'#w0rkDrawWrap', contents:
               [
                  {div:'#w0rkEditDraw'}
               ]}
            ]);

            self.vars.file.mime=this.src.stub(':')[2].stub(';')[0];
            self.vars.canvas=(new Konva.Stage({container:'w0rkEditDraw', width:(this.width*4), height:(this.height*4)}));
            self.vars.activeLayer=(new Konva.Layer()); self.vars.canvas.add(self.vars.activeLayer); self.vars.selected=[]; self.feed(this.src);
            self.vars.canvas.width(this.width); self.vars.canvas.height(this.height);

            self.vars.canvas.on('mousedown',function(evnt)
            {
               let o=evnt.target; if(o.attrs.name&&o.attrs.name.hasAny(' _anchor')){return};
               if(o.parent&&(o.parent.nodeType=='Group')){o=o.parent}; let c=w0rk.edit.draw.vars.canvas; if(!o.parent||!evnt.evt.ctrlKey)
               {c.find('Transformer').destroy(); c.children.forEach((i)=>{i.draw()}); if(!o.parent){w0rk.edit.draw.vars.selected=[];return}};
               if(!evnt.evt.ctrlKey){c.find('Transformer').destroy(); o.parent.draw(); w0rk.edit.draw.vars.selected=[]};
               let f=(new Konva.Transformer()); o.parent.add(f); f.attachTo(o); o.parent.draw();
               w0rk.edit.draw.vars.selected.push(o);
            });

            Select('#w0rkDrawWrap').onFeed(w0rk.edit.draw.feed);
            Listen('keydown',function(evnt)
            {
               let cmbo=evnt.signal; let tool=w0rk.edit.draw.tool; let code=evnt.code;
               if((cmbo=='Delete')||(cmbo=='Backspace')){tool.remove();return};
               if(!evnt.ctrlKey){return}; evnt.preventDefault(); evnt.stopPropagation();
               if(cmbo=='Control g'){tool.enClan();return}; if(cmbo=='Control u'){tool.deClan();return};
               if(code=='ArrowUp'){tool.zLevel('moveUp');return}; if(code=='ArrowDown'){tool.zLevel('moveDown');return};
               if(cmbo=='Control s'){tool.save();return};
            });
            Listen('mousewheel',function(evnt)
            {
               if(!evnt.ctrlKey){return}; evnt.preventDefault(); evnt.stopPropagation();
               let x=(evnt.deltaX+''); let y=(evnt.deltaY+''); if(x=='-0'){x='0'}; if(y=='-0'){y='0'};
               x=(x.split('.')[0]*1); y=(y.split('.')[0]*1); let tool=w0rk.edit.draw.tool;
               if(!evnt.shiftKey){tool.zoom(y);return}; tool.crop(y,x);
            });

            let wrap=Select('#w0rkDrawWrap'); wrap.style.width=(this.width+'px'); wrap.style.height=(this.height+'px');
            wrap.style.display='inline-block'; wrap.style.overflow='hidden'; let face=Select('#w0rkEditView');
            face.style.position='relative'; face.style.width=(panl.width+'px'); face.style.height=(panl.height+'px');
            face.style.overflow='auto';
         });
      },


      feed:function(v, m,l,o,f,self)
      {
         self=w0rk.edit.draw; m=v.stub(':')[2].stub(';')[0]; l=self.vars.activeLayer; self.vars.canvas.find('Transformer').destroy();

         if(m.hasAny('image')){Create({img:v, onload:function()
         {
            o=(new Konva.Image({x:0, y:0, image:this, width:this.width, height:this.height, draggable:true}));
            l.add(o); f=(new Konva.Transformer()); l.add(f); f.attachTo(o); l.draw(); self.vars.selected=[o];
         }});return};

         dump(m);
      },

      tool:
      {
         remove:function(s,self)
         {
            self=w0rk.edit.draw; s=self.vars.selected; self.vars.canvas.find('Transformer').destroy();
            s.forEach(function(o){let p=o.parent; o.destroy(); p.draw()}); self.vars.selected=[];
         },

         enClan:function(s,g,l,f,self)
         {
            self=w0rk.edit.draw; s=self.vars.selected; g=(new Konva.Group({x:0,y:0,draggable:true})); l=self.vars.activeLayer;
            self.vars.canvas.find('Transformer').destroy(); s.forEach((o)=>{o.draggable(false); o.moveTo(g)});
            l.add(g); f=(new Konva.Transformer()); l.add(f); f.attachTo(g); f.forceUpdate(); l.draw(); self.vars.selected=[g];
         },

         deClan:function(s,self)
         {
            self=w0rk.edit.draw; s=self.vars.selected; self.vars.selected=[]; self.vars.canvas.find('Transformer').destroy();
            s.forEach(function(g)
            {
               if(g.nodeType!='Group'){return}; let p=g.parent; g.children.forEach((o)=>
               {o.draggable(true); o.moveTo(p); let f=(new Konva.Transformer()); p.add(f); f.attachTo(o);}); if(p.draw)(p.draw());
            });
         },

         zLevel:function(d, self,l,o)
         {
            self=w0rk.edit.draw; l=self.vars.activeLayer; o=self.vars.selected[0]; if(!o){return};
            o[d](); l.draw();
         },

         zoom:function(v, face,wrap)
         {
            face=w0rk.edit.draw.vars.canvas; face.find('Transformer').destroy(); let os,sb,ns,nw,nh,co,kc,ce,br;
            os=face.scaleX(); sb=((v/1000)/2); ns=(os+sb); if(ns<0){ns=0}; face.scale({x:ns,y:ns});
            br=face.getClientRect(); wrap=Select('#w0rkDrawWrap'); nw=br.width; nh=br.height;
            face.width(nw); face.height(nh); wrap.style.width=(nw+'px'); wrap.style.height=(nh+'px'); face.batchDraw();
         },

         crop:function(vy,vx, face,wrap)
         {
            face=w0rk.edit.draw.vars.canvas; wrap=Select('#w0rkDrawWrap'); face.find('Transformer').destroy(); let os,ns,br,sb,nw,nh;
            br=(wrap.cb?{width:face.width(),height:face.height()}:face.getClientRect()); wrap.cb=1; nw=Math.trunc(br.width+(vx/2)); nh=Math.trunc(br.height+(vy/2));
            face.width(nw); face.height(nh); wrap.style.width=(nw+'px'); wrap.style.height=(nh+'px'); face.batchDraw();
         },

         save:function(face,file,durl)
         {
            face=w0rk.edit.draw.vars.canvas; file=w0rk.edit.draw.vars.file; face.find('Transformer').destroy();
            durl=face.toDataURL({mimeType:file.mime,quality:0.9});
            purl({target:'/w0rk/edit/draw/save', method:'POST', convey:{path:file.path,data:durl}},function()
            {
               dump(this.echo.body);
            });
         },
      }
   }
});
