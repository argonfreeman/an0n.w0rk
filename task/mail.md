## Thank you {:(fromName):}

Your docket has been dispatched to the team for processing.

- You will automatically receive workflow progress updates to this email address
- Your reference: `About #{:(docketID):} -` in the subject of this email refers to this docket directly -only in the subject
- If you want to communicate with the team regarding this docket, just reply to this email -keeping the subject intact
- If you want to cancel this docket, then reply -but change the subject to begin with this: `Cancel #{:(docketID):} -` exactly

***below is a copy of what we received***

***


{:(mesgBody):}
