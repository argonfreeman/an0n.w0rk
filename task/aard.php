<?
namespace Anon;




class task
{
   static $meta;



   static function init()
   {
      // if(NAVIPATH==='/w0rk/task/init'){foo();};
      if(NAVIPATH !== '/c0r3/lib/core/events/enhook'){render::parsed('/w0rk/task/base.js'); return;};
      emit('test');
   }



   static function getBoard()
   {
      $r=[]; $c=User(USERNAME,'role'); $h='/w0rk/task/list'; $d=path::scan($h); if(!$c||(span($d)<1)){return $r;};

      foreach($d as $x)
      {
         $p="{$h}{$x}"; $wc=path::scan("$p/withClan"); $wu=path::scan("$p/withUser"); if(!isin($wc,$c)||($wu&&($wu!==$u))){continue;};
         $l=filter(path::scan($p),function($i){return (ltrim($i,'/'));});
         $o=[]; foreach($l as $i)
         {
            if(isFile("$p/$i")){$o[$i]=path::scan("$p/$i"); continue;}; if($i!=='comments'){continue;}; $o[$i]=[];
            $cl=filter(path::scan("$p/$i"),function($ci){return (ltrim($ci,'/'));}); if(span($cl)<1){continue;}; arsort($cl);
            $cl=array_values($cl); $fc=$cl[0]; $ck=filter(path::scan("$p/$i/$fc"),function($i){if(!isin($i,'.')){return (ltrim($i,'/'));}});
            $co=[]; foreach($ck as $cp){$co[$cp]=path::scan("$p/$i/$fc/$cp");}; $o[$i][]=$co;
         };
         $r[]=$o;
      };

      return $r;
   }



   static function getEmail()
   {
   }



   static function makeNote($o)
   {
      if(!isTron($o)||(isTron($o)&&!locate(keys($o),['dref','user','mail','mesg'],XACT))){fail('invalid comment args');};
      if(!$o->cref){$o->cref=substr((swap(BOOTTIME,'.','').random(10)),0,20);}; $h="/w0rk/task/list/$o->dref/comments/$o->cref";
      path::make("$h/mesg",$o->mesg,FILE); path::make("$h/user",$o->user,FILE);  path::make("$h/nick",($o->nick?$o->nick:''),FILE);
      path::make("$h/mail",$o->mail,FILE); path::make("$h/time",microtime(true),FILE); path::make("$h/rate",'0',FILE);
      $p="sqlite::/w0rk/user/$o->user/data"; $d=target($p); $d->vivify(); $r=$d->select([using=>"rate",fetch=>"numr",where=>"mail = $o->mail"]);
      if(!isset($r[0])){$d->insert([using=>'rate',write=>[$o->mail,0]]);}; $d->pacify(); return true;
   }



   static function makeDokt()
   {
      $v=vars('client'); if(!$v->dref){$v->dref=substr((encode::b62((swap(BOOTTIME,'.','')*1)).random(8)),0,12);};
      if(!$v->cref){$v->cref=substr((swap(BOOTTIME,'.','').random(10)),0,20);};
      $h="/w0rk/task/list/$v->dref"; $cr=$v->cref; $cp="$h/comments/$cr";
       //if(!isFold($h)){path::make($h,FOLD);};
      if(!isFold($cp)){path::make($cp,FOLD);};


      if($v->type==='upload')
      {
         path::make("$cp/$v->file",$v->durl['body'],FILE);
         path::make("$h/temp",'!',FILE);
         emit(encode::json(['dref'=>$v->dref,'cref'=>$v->cref]));
      };


      if($v->type==='create')
      {
         $v->docketID=$v->dref; $b=import('/w0rk/task/mail.md',$v);  $bw='<img src="';  $ew='" alt="';
         requires('/c0r3/lib/edit/Parsedown.php'); $pd=(new \Parsedown()); $pd->setBreaksEnabled(true); $b=$pd->text($b);
         $il=expose($b,$bw,$ew); if($il){foreach($il as $in){$f="{$bw}{$in}{$ew}"; $r="{$bw}{$cp}/{$in}{$ew}"; $b=swap($b,$f,$r);}};

         $b=import('c0r3/lib/mail/page.htm',['html'=>$b]); if(!online()){emit('server connection issue, please try again');};
         if(!$v->mesgHead){$v->mesgHead=HOSTNAME;};

         $r=within(MAILFROM)->insert
         ([
            'destAddy'=>$v->fromAddy,
            'mesgHead'=>"About #$v->dref - $v->mesgHead",
            'mesgBody'=>$b,
         ]);

         if($r->fail){emit("sending confirmation to `$v->fromAddy` failed, make sure the address exists, or try again");};
         unset($v->type); $v->initTime=BOOTTIME; $v->editTime=microtime(true); $v->fromUser=USERNAME; $v->withClan='sorter'; $v->withUser='';
         $v->progress='todo'; $v->docketID=$v->hash; $v->handlers=''; $v->ancestry=''; $dr=$v->dref; $cr=$v->cref; $cm=$v->mesgBody;
         unset($v->dref,$v->cref,$v->mesgBody); foreach($v as $n => $d){path::make("$h/$n",$d,FILE);};
         dComment(tron(['dref'=>$dr, 'cref'=>$cr, 'user'=>USERNAME, 'nick'=>$v->fromName, 'mail'=>$v->fromAddy, 'mesg'=>$cm]));
         path::void("$h/temp"); emit('OK');
      };
   }



   static function rateUser()
   {
   }



   static function dispense()
   {
      // $tl=self::getBoard();
      return 'Hello World!';
      // $ml=self::getEmail();
   }
}




// TODO :
      // if(connection_aborted()){break;}; $nl=encode::json(taskList(USERNAME));
      // $oh=sha1($ol); $nh=sha1($nl); if($oh===$nh){echo ": ping\n\n";continue;}; $ol=$nl;
      // $ed=encode::b64($nl); echo "id:$id\nevent: todo\ndata: $ed\n\n"; flush(); wait($ti);
