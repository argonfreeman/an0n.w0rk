"use strict";


// w0rk :: task : tool
// --------------------------------------------------------------------------------------------------------------------------------------------
   requires(['/w0rk/task/skin.css'],()=>{});

   Extend(w0rk)
   ({
      task:
      {
         vars:{active:undefined},


         init:function()
         {
            Server.Listen('task','/w0rk/task/dispense',function(evnt)
            {
               dump(evnt);
            });
            // this.ping = (new EventSource('/w0rk/task/ping',{withCredentials:true}));
            // this.ping.Listen('open',function(evnt){});
            // this.ping.Listen('error',function(evnt){});
            // this.ping.Listen('todo',function(evnt)
            // {
            //    let l=decode.JSON(decode.b64(evnt.data)); var task=Select('#todoW0rkList'); if(!task){return};
            //    if(l.length<1){l=task.Select('.doktItem'); l.forEach((i)=>{i.Delete()}); return};
            //    l.forEach((i)=>{if(task.Select('#dokt_'+i.docketID)){return}; task.Insert(doktItem(i))});
            // });
            // // server.close();
         },
      }
   });
// --------------------------------------------------------------------------------------------------------------------------------------------




// w0rk :: task : slab
// --------------------------------------------------------------------------------------------------------------------------------------------
   Export
   ([
      {grid:'#taskD3vlPanl .w0rkPanlSlab', onready:function(){w0rk.task.init()}, contents:
      [
         {row:
         [
            {col:
            [
               {grid:
               [
                  {row:[{col:'.slabMenuHead', contents:'todo'}]},
                  {row:[{col:'.panlHorzLine', contents:{hdiv:''}}]},
                  {row:[{col:'.slabMenuBody', contents:{panl:'#todoW0rkList'}}]},
               ]}
            ]},
            {col:'.panlVertDlim', style:'max-width:5px', contents:{vdiv:''}},
            {col:
            [
               {grid:
               [
                  {row:[{col:'.slabMenuHead', contents:'busy'}]},
                  {row:[{col:'.panlHorzLine', contents:{hdiv:''}}]},
                  {row:[{col:'.slabMenuBody', contents:'',}]},
               ]}
            ]},
            {col:'.panlVertDlim', style:'max-width:5px', contents:{vdiv:''}},
            {col:
            [
               {grid:
               [
                  {row:[{col:'.slabMenuHead', contents:'hold'}]},
                  {row:[{col:'.panlHorzLine', contents:{hdiv:''}}]},
                  {row:[{col:'.slabMenuBody', contents:'',}]},
               ]}
            ]},
            {col:'.panlVertDlim', style:'max-width:5px', contents:{vdiv:''}},
            {col:
            [
               {grid:
               [
                  {row:[{col:'.slabMenuHead', contents:'test'}]},
                  {row:[{col:'.panlHorzLine', contents:{hdiv:''}}]},
                  {row:[{col:'.slabMenuBody', contents:'',}]},
               ]}
            ]},
            {col:'.panlVertDlim', style:'max-width:5px', contents:{vdiv:''}},
         ]}
      ]}
   ]);
// --------------------------------------------------------------------------------------------------------------------------------------------




// func :: doktItem : returns doctet node-object from given docket data
// --------------------------------------------------------------------------------------------------------------------------------------------
   Extend(Main)
   ({
      doktItem:function(o, r,p,c,m,l,q)
      {
         p=o.priority.trim(); c=((p=='threat')?0:((p=='urgent')?50:220)); q=o.comments[0]; m=q.mesg;
         p=m.stub('\n'); if(p&&p[0].hasAny('# '+o.mesgHead)){m=p[2].trim()};
         l=(m.expose('![',')','](')||[]); l.forEach((i)=>{m=m.swap(('!['+i+')'),'[image]')});
         l=(m.expose('[',')','](')||[]); l.forEach((i)=>{m=m.swap(('['+i+')'),'[link]')});
         m=((m.length>60)?(m.substr(0,128)+'...'):m);

         r={grid:('#dokt_'+o.docketID+' .doktItem') ,contents:
         [
            {row:
            [
               {col:'.doktItemHead', contents:
               [
                  {div:'', style:
                  {
                     display:'inline-block', width:'60px', height:'10px', background:('hsla('+c+',100%,50%,1)'), transform:'isoSkewX(-30deg)',
                     border:('1px solid hsla('+c+',100%,100%,1)'), borderTop:'none',
                  }},
               ]},
            ]},
            {row:
            [
               {col:'.doktItemBody', style:{position:'relative'}, contents:
               [
                  {grid:'', style:'margin-top:-6px', contents:
                  [
                     {row:
                     [
                        {col:'.doktItemFromFace', title:(o.fromUser+' '+(o.fromName?('['+o.fromName+']'):'')), contents:
                        [
                           {img:('/w0rk/user/'+o.fromUser+'/data/face.jpg')},
                           {div:'.doktItemFromRank'},
                        ]},
                        {col:'.doktItemMesgHead',contents:
                        [
                           {div:'', contents:[{h4:o.mesgHead}, {div:m}]},
                        ]},
                     ]},
                  ]},
                  {div:'.doktItemTime', contents:
                  (((q.user==q.nick)?q.user:(q.user+' ['+q.nick+']'))+' - '+timePast((q.time*1),{:(BOOTTIME):}))},
               ]},
            ]},
            {row:
            [
               {col:'.doktItemFoot', contents:
               [
                  {grid:'.doktItemFootGrid', style:'margin-top:-14px', contents:
                  [
                     {row:
                     [
                        {col:'.doktItemFootLine'},
                        {col:'.doktItemFootButn', contents:
                        [
                           {div:'.doktItemFootButn', contents:
                           [
                              {div:'', style:
                              {
                                 width:'60px', height:'12px', background:('hsla(0,0%,93%,1)'), transform:'isoSkewX(30deg)',
                                 border:'1px solid hsla(0,0%,79%,1)', borderBottom:'none',
                                 position:'absolute', bottom:'-1px',
                              }}
                           ]},
                        ]},
                        {col:'.doktItemFootLine'},
                     ]},
                  ]},
                  {div:'.doktItemFootData', contents:''},
                  {div:'', style:'height:6px; background:hsla(0,0%,93%,1)'},
               ]},
            ]},
         ]};
         return r;
      },
   });
// --------------------------------------------------------------------------------------------------------------------------------------------
