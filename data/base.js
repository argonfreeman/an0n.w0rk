
// w0rk :: data : tool
// --------------------------------------------------------------------------------------------------------------------------------------------
   requires(['/w0rk/data/skin.css'],()=>{});

   Extend(w0rk)
   ({
      data:
      {
         vars:{active:undefined},


         init:function()
         {
         },
      }
   });
// --------------------------------------------------------------------------------------------------------------------------------------------




// w0rk :: data : slab
// --------------------------------------------------------------------------------------------------------------------------------------------
   Export
   ([
      {grid:'#dataD3vlPanl .w0rkPanlSlab', onready:function(){w0rk.data.init()}, contents:
      [
         {row:
         [
            {col:'.panlSlabMenu', contents:
            [
               {grid:
               [
                  {row:[{col:'.slabMenuHead', contents:'database'}]},
                  {row:[{col:'.panlHorzLine', contents:{hdiv:''}}]},
                  {row:[{col:'.slabMenuBody', contents:'',}]},
               ]}
            ]},
            {col:'.panlVertDlim', contents:{vdiv:'.moveHorz', target:'^<'}},
            {col:'.panlSlabView', contents:
            [
               {grid:
               [
                  {row:[{col:'.slabViewHead', contents:[]}]},
                  {row:[{col:'.panlHorzLine', contents:{hdiv:''}}]},
                  {row:[{col:'.slabViewBody', contents:[{panl:'#w0rkTaskView'}]}]},
               ]}
            ]},
         ]}
      ]}
   ]);
// --------------------------------------------------------------------------------------------------------------------------------------------
