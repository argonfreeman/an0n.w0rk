
   requires(['/w0rk/help/skin.css'],()=>{});

// w0rk :: help : tool
// --------------------------------------------------------------------------------------------------------------------------------------------
   Extend(w0rk)
   ({
      help:
      {
         vars:{active:undefined},


         init:function()
         {
            // dump('olo from help');
         },


         open:function(b)
         {
            if(!!this.vars.active){this.vars.active.declan('slabMenuActv'); delete this.vars.active;};
            let v=b.id.substr(2); Select('#w0rkHelpView').innerHTML=''; b.enclan('slabMenuActv'); w0rk.help.vars.active=b;
            Render(('/w0rk/help/open/'+v),Select('#w0rkHelpView'));
         },
      }
   });
// --------------------------------------------------------------------------------------------------------------------------------------------




// w0rk :: help : slab
// --------------------------------------------------------------------------------------------------------------------------------------------
   Export
   ([
      {grid:'#helpD3vlPanl .w0rkPanlSlab', onready:function(){w0rk.help.init()}, contents:
      [
         {row:
         [
            {col:'.panlSlabMenu', contents:
            [
               {grid:
               [
                  {row:[{col:'.slabMenuHead', contents:'help'}]},
                  {row:[{col:'.panlHorzLine', contents:{hdiv:''}}]},
                  {row:[{col:'.slabMenuBody', contents:{:(menu):},}]},
               ]}
            ]},
            {col:'.panlVertDlim', contents:{vdiv:'.moveHorz', target:'^<'}},
            {col:'.panlSlabView', contents:
            [
               {grid:
               [
                  {row:[{col:'.slabViewHead', contents:[]}]},
                  {row:[{col:'.panlHorzLine', contents:{hdiv:''}}]},
                  {row:[{col:'.slabViewBody', contents:[{panl:'#w0rkHelpView'}]}]},
               ]}
            ]},
         ]}
      ]}
   ]);
// --------------------------------------------------------------------------------------------------------------------------------------------
