## Hi {:(username):}!

You have been welcomed to the following clans:
**{:(clanlist):}**

To identify yourself, use the following credentials:
- username: `{:(username):}`
- password: `{:(password):}`

You can change your password to something easy to remember, like: `R00k13!`
If you are told to RTFM, just login and go here: https://{:(HOSTNAME):}/w0rk/help

It dawned upon the powers that be that you may need to know ***how to login***
- use you favorite browser and go here: https://{:(HOSTNAME):}
- on your keyboard press the `Ctrl ~` keys together
- in the command console that pops up, type `su {:(username):}` and hit the `Enter` key
- now you are prompted for your password, just enter your password .. copy & paste works
- if all went well, you should no longer be anonymous .. well done ;)
- now enter `help` .. or hit the book icon on the panel menu

Welcome to the team!
