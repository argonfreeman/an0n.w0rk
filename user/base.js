
// w0rk :: user : tool
// --------------------------------------------------------------------------------------------------------------------------------------------
   requires(['/w0rk/user/skin.css'],()=>{});

   Extend(w0rk)
   ({
      user:
      {
         vars:{active:undefined},


         init:function()
         {
         },
      }
   });
// --------------------------------------------------------------------------------------------------------------------------------------------




// w0rk :: user : slab
// --------------------------------------------------------------------------------------------------------------------------------------------
   Export
   ([
      {grid:'#userD3vlPanl .w0rkPanlSlab', onready:function(){w0rk.user.init()}, contents:
      [
         {row:
         [
            {col:'.panlSlabMenu', contents:
            [
               {grid:
               [
                  {row:[{col:'.slabMenuHead', contents:'users'}]},
                  {row:[{col:'.panlHorzLine', contents:{hdiv:''}}]},
                  {row:[{col:'.slabMenuBody', contents:'',}]},
               ]}
            ]},
            {col:'.panlVertDlim', contents:{vdiv:'.moveHorz', target:'^<'}},
            {col:'.panlSlabView', contents:
            [
               {grid:
               [
                  {row:[{col:'.slabViewHead', contents:[]}]},
                  {row:[{col:'.panlHorzLine', contents:{hdiv:''}}]},
                  {row:[{col:'.slabViewBody', contents:[{panl:'#w0rkTaskView'}]}]},
               ]}
            ]},
         ]}
      ]}
   ]);
// --------------------------------------------------------------------------------------------------------------------------------------------
