
// w0rk :: tool : tool
// --------------------------------------------------------------------------------------------------------------------------------------------
   requires(['/w0rk/tool/skin.css'],()=>{});

   Extend(w0rk)
   ({
      tool:
      {
         vars:{active:undefined},


         init:function()
         {
            // dump('olo from tool');
         },


         open:function(b)
         {
            if(!!this.vars.active){this.vars.active.declan('slabMenuActv'); delete this.vars.active;};
            let v=(b.Select('span')[0].innerHTML).swap(' ','_'); Select('#w0rkToolView').innerHTML='';
            Render(('/w0rk/tool/open/'+v),Select('#w0rkToolView'),function(){b.enclan('slabMenuActv'); w0rk.tool.vars.active=b;});
         },
      }
   });
// --------------------------------------------------------------------------------------------------------------------------------------------




// w0rk :: tool : slab
// --------------------------------------------------------------------------------------------------------------------------------------------
   Export
   ([
      {grid:'#toolD3vlPanl .w0rkPanlSlab', onready:function(){w0rk.tool.init()}, contents:
      [
         {row:
         [
            {col:'.panlSlabMenu', contents:
            [
               {grid:
               [
                  {row:[{col:'.slabMenuHead', contents:'tools'}]},
                  {row:[{col:'.panlHorzLine', contents:{hdiv:''}}]},
                  {row:[{col:'.slabMenuBody', contents:{:(menu):},}]},
               ]}
            ]},
            {col:'.panlVertDlim', contents:{vdiv:'.moveHorz', target:'^<'}},
            {col:'.panlSlabView', contents:
            [
               {grid:
               [
                  {row:[{col:'.slabViewHead', contents:[]}]},
                  {row:[{col:'.panlHorzLine', contents:{hdiv:''}}]},
                  {row:[{col:'.slabViewBody', contents:[{panl:'#w0rkToolView'}]}]},
               ]}
            ]},
         ]}
      ]}
   ]);
// --------------------------------------------------------------------------------------------------------------------------------------------
