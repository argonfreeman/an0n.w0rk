<?
namespace Anon;
?>

<style>
   .iconViewItem
   {
      display:inline-block; position:relative;
      width:136px; height:75px; padding:5px; text-align:center;
      background:hsla(0,0%,50%,0.1); overflow:hidden;
      border:1px solid hsla(0,0%,50%,0.3); border-radius:2px;
      margin-left:10px; margin-top:10px;
      cursor:default;
   }
   .iconViewItem:hover
   {
      color:hsla(0,0%,90%,1);
      background:hsla(0,0%,90%,0.1);
      border:1px solid hsla(0,0%,90%,0.3);
   }

   .iconViewFace
   {
      font-family:'icon';
      font-size:36px; line-height:36px;
   }

   .iconViewText
   {
      display:block; position:absolute; left:0px; bottom:0px; width:100% !important; height:20px !important; padding:2px !important;
      font-family:'butn'; font-size:12px; color:hsla(0,0%,60%,1) !important; text-align:center !important;
      background:transparent; background:hsla(0,0%,10%,0.1) !important; margin:0px !important;
      outline:none !important; border:none !important; border-top:1px solid hsla(0,0%,50%,0.3) !important;
   }
</style>

<?
   $l=frag(trim(stub(path::scan('/c0r3/fnt/icon.fnt'),'/**********/')[2]),"\n");
   foreach($l as $i)
   {
      $n=stub(stub($i,':')[0],'-')[2];
      ?>
         <div class="iconViewItem" onclick="copyToClipboard(this.Select('.iconViewText')[0].value); this.style.border='1px solid #2266FF'">
            <div class="iconViewFace icon-<?=$n?>"></div>
            <input class="iconViewText" value="<?=$n?>" />
         </div>
      <?
   };
