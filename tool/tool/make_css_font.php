<?
namespace Anon;

if(USERDEED==='select')
{
   ?>
      <script>
         Upload.create({parent:'#w0rkHackView',submit:function(fc,fn,fv,s,i)
         {
            if(!this.fields){this.fields={files:{},paths:{}}}; if(!this.got){this.got={}}; s=this;
            i=s.Select(".dropZoneInfo")[0]; i.style.color='hsla(0,0%,50%,1)'; i.style.fontSize='11px';

            if((fc=='files')&&fv.hasAny('/css;')){s.got.css=1; s.fields.files[fn]=fv; i.innerHTML+="<br>- got css -";};
            if((fc=='files')&&fv.hasAny(':font/')){s.got.fnt=1; s.fields.files[fn]=fv; i.innerHTML+="<br>- got the font -";};
            if((fc=='paths')&&isPath(fv)){s.got.pth=1; s.fields.paths[fn]=fv;};

            if(!s.got.fnt&&!s.got.pth&&!s.got.css){s.got.all=0; i.innerHTML="drop a font file .. css file is optional"; return};
            if(s.got.fnt&&!s.got.pth)
            {
               setTimeout(function()
               {
                  if(s.Select('input')[0]){return};
                  i.innerHTML+='<br><br>enter desination dir then hit Enter/Return:<br>'; let n=Create('input');
                  n.placeholder='/path/to/writable/dir'; n.style.background='transparent'; n.style.margin='5px'; n.style.borderStyle='solid';
                  n.style.borderWidth='1px'; n.style.border='1px solid hsla(0,0%,60%,1);'; n.style.color='hsla(0,0%,60%,1)';
                  n.onkeypress=function(e){n.style.border='1px solid hsla(0,0%,60%,1);'; if(e.keyCode==13)
                  {
                     let p=this.value; if(!isPath(p)){this.style.border='2px solid red'; return};
                     this.style.border='2px solid green'; s.submit('paths','toDir',p);
                  }};
                  i.appendChild(n);
               },50);
            }

            if(s.got.fnt&&s.got.pth){s.got.all++;};
            if(s.got.all!==1){return;};

            purl({target:'/w0rk/hack/tool/make_css_font',method:'POST',convey:s.fields},function()
            {
               if(this.response==='OK'){i.innerHTML+='<br><br>:: DONE ::'; return;};
               dump(this.response);
            });
         }});
      </script>
   <?
};


if(USERDEED==='insert')
{
   $o=vars('client'); $td=rtrim($o->paths->toDir,'/'); $o=$o->files; $fn=''; $cf=''; $ff='';
   foreach($o as $k => $v){if(strpos($v,'/css;')){$cf=$v;}else{$p=frag($k,'.'); rpop($p); $fn=fuse($p,'.'); $ff=$v;};};
   $mt=stub(stub($ff,';base64,')[0],':')[2]; $ft=stub($mt,'/')[2];

   $rf="@font-face{font-family:'$fn'; src:url('$ff') format('$ft'); font-weight:normal; font-style:normal;}\n\n"
      .".font-$fn{font-family:'$fn';}";

   if($cf)
   {
      $cf=trim(remove(decode::b64(frag($cf,';base64,')[1]),'/*','*/')); $ai=indx($cf,'@font-face');
      if(($ai!==null)&&($ai!==0)){fail('`@font-face` is expected at beginning .. or not at all .. comments are OK');}; $ai=true;
      if($ai){$cp=stub($cf,'}'); $cf=trim($cp[2]);}; if(indx($cf,'[class^=')!==null){$cp=stub($cf,'}'); $cf=trim($cp[2]);};
      $cf=swap($cf,[' {',"\n  "],['{',"\n"]); $cf=trim(swap($cf,["{\n",": ",";\n}"],["{",":",";}"])); $cf=impose($cf,'.','-',$fn);
      if($ai)
      {
         $rf.="\n\n[class^='$fn-']:before, [class*=' $fn-']:before{font-family:'$fn' !important; speak:none; "
             ."font-style:normal; font-weight:normal; font-variant:normal; text-transform:none; -webkit-font-smoothing:antialiased; "
             ."-moz-osx-font-smoothing:grayscale;}\n\n/**********/\n\n$cf";
      }
   }


   path::make("$td/$fn.fnt",$rf); emit('OK');
};
